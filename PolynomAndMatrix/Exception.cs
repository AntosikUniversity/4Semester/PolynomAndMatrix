﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolynomAndMatrix
{
    /// <summary>
    /// Our class, inherited from Exception
    /// </summary>
    /// <seealso cref="System.Exception" />
    class MyException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MyException"/> class.
        /// </summary>
        public MyException()
        {
            Console.WriteLine("ERROR!");
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="MyException"/> class.
        /// </summary>
        /// <param name="message">Message with error description.</param>
        public MyException(string message) : base(String.Format("ERROR! Description: {0}", message))
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="MyException"/> class.
        /// </summary>
        /// <param name="message">Message with error description.</param>
        /// <param name="inner">The inner exception.</param>
        public MyException(string message, Exception inner) : base(String.Format("ERROR! Description: {0}", message), inner)
        {
        }
    }
}
