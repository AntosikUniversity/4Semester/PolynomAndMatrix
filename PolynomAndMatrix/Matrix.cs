﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolynomAndMatrix
{
    /// <summary>
    /// Our matrix class
    /// </summary>
    /// <seealso cref="System.ICloneable" />
    /// <seealso cref="System.Collections.IEnumerable" />
    class Matrix : ICloneable, IEnumerable
    {
        double[,] data;
        int size;

        /// <summary>
        /// Initializes a new instance of the square <see cref="Matrix"/> class.
        /// </summary>
        public Matrix()
        {
            Random rand = new Random();
            size = rand.Next(1, 5);
            data = new double[size, size];
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    data[i, j] = rand.Next(1, 30);
        }
        /// <summary>
        /// Initializes a new instance of the square <see cref="Matrix"/> class.
        /// </summary>
        /// <param name="sz">The size.</param>
        /// <exception cref="PolynomAndMatrix.MyException">Size must be greater than 0!</exception>
        public Matrix(int sz = 0)
        {
            if (sz <= 0) throw new MyException("Size must be greater than 0!");
            Random rand = new Random();
            size = sz;
            data = new double[size, size];
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    data[i, j] = rand.Next(1, 30);
        }
        /// <summary>
        /// Initializes a new instance of the square <see cref="Matrix"/> class.
        /// </summary>
        /// <param name="dt">The data (two-dimension array).</param>
        /// <exception cref="PolynomAndMatrix.MyException">Matrix must be square like!</exception>
        public Matrix(double[,] dt)
        {
            if (!isSquareMatrix(dt)) throw new MyException("Matrix must be square like!");
            size = dt.GetUpperBound(0) + 1;
            data = dt;
        }


        /// <summary>
        /// Implements the operator +.
        /// </summary>
        /// <param name="c1">The matrix #1.</param>
        /// <param name="c2">The matrix #2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Matrix operator +(Matrix c1, Matrix c2)
        {
            int size = c1.size;
            double[,] newdata = new double[size, size];
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    newdata[i, j] = c1.data[i, j] + c2.data[i, j];
            return new Matrix(newdata);
        }
        /// <summary>
        /// Implements the operator -.
        /// </summary>
        /// <param name="c1">The matrix #1.</param>
        /// <param name="c2">The matrix #2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Matrix operator -(Matrix c1, Matrix c2)
        {
            int size = c1.size;
            double[,] newdata = new double[size, size];
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    newdata[i, j] = c1.data[i, j] - c2.data[i, j];
            return new Matrix(newdata);
        }

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="c1">The matrix #1.</param>
        /// <param name="c2">The matrix #2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Matrix operator *(Matrix c1, Matrix c2)
        {
            int size = c1.size;
            double[,] newdata = new double[size, size];
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    for (int r = 0; r < size; r++)
                        newdata[i, j] += c1.data[i, r] * c2.data[r, j];
            return new Matrix(newdata);
        }

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="c1">The matrix.</param>
        /// <param name="c2">The number.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Matrix operator *(Matrix c1, double c2)
        {
            int size = c1.size;
            double[,] newdata = new double[size, size];
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    newdata[i, j] += c1.data[i, j] * c2;
            return new Matrix(newdata);
        }

        /// <summary>
        /// Implements the operator /.
        /// </summary>
        /// <param name="c1">The matrix #1.</param>
        /// <param name="c2">The matrix #2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Matrix operator /(Matrix c1, Matrix c2)
        {
            double det = c2.Determinant();
            return c1 * (c2.Transpose() * (1.0 / det));
        }

        /// <summary>
        /// Transposes our matrix.
        /// </summary>
        /// <returns>
        /// New <see cref="Matrix"/>
        /// </returns>
        public Matrix Transpose()
        {
            double[,] newdata = new double[size, size];
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    newdata[i, j] = data[j, i];
            return new Matrix(newdata);
        }

        /// <summary>
        /// Returns Determinant of current Matrix
        /// </summary>
        /// <returns>
        /// int Determinant
        /// </returns>
        public double Determinant()
        {
            double[,] arrn = this.data;
            double result = 1;
            for (int i = 0; i < size - 1; i++)
            {
                for (int j = i + 1; j < size; j++)
                {
                    double koef = arrn[j, i] / arrn[0, i];
                    for (int k = i; k < size; k++)
                    {
                        double n1 = arrn[j, k];
                        double n2 = arrn[i, k];
                        arrn[j, k] = n1 - Math.Abs(koef) * n2;
                    }
                    arrn[j, i] = 0;
                }

            }
            for (int p = 0; p < size; p++)
                result = result * arrn[p, p];
            return result;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents our <see cref="Matrix"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents our <see cref="Matrix"/>.
        /// </returns>
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                    result.Append(String.Format("{0}\t", data[i, j]));
                result.AppendLine();
            }
            return result.ToString();
        }

        /// <summary>
        /// Creates copy of our <see cref="Matrix"/>.
        /// </summary>
        /// <returns>
        /// New <see cref="Matrix"/>, the copy of our <see cref="Matrix"/>.
        /// </returns>
        public object Clone()
        {
            return new Matrix(data);
        }

        /// <summary>
        /// Determines whether [is square matrix] from data (two-dimension array).
        /// </summary>
        /// <param name="dt">The data.</param>
        /// <returns>
        ///   <c>true</c> if [is square matrix] from data; otherwise, <c>false</c>.
        /// </returns>
        private bool isSquareMatrix(double[,] dt)
        {
            if (dt.Rank > 1)
            {
                int x = dt.GetUpperBound(0) + 1;
                int y = dt.GetUpperBound(1) + 1;
                if (x == y) return true;
                else return false;
            }
            else return false;
        }

        /// <summary>
        /// Returns IEnumerator
        /// </summary>
        /// <returns>
        /// Object <see cref="T:System.Collections.IEnumerator" />
        /// </returns>
        public IEnumerator GetEnumerator()
        {
            return data.GetEnumerator();
        }
    }
}
