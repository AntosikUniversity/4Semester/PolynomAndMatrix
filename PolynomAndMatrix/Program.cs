﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolynomAndMatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            double[,] data = { { 1, 2 }, { 3, 4 } };
            double[,] data2 = { { 4, 3 }, { 2, 1 } };
            Matrix n1 = new Matrix(data);
            Matrix n2 = new Matrix(data2);

            Console.WriteLine("Matrix #1");
            Console.WriteLine(n1);
            Console.WriteLine("Matrix #2");
            Console.WriteLine(n2);
            Console.WriteLine("Matrix #1 + #2");
            Console.WriteLine(n1 + n2);
            Console.WriteLine("Matrix #1 - #2");
            Console.WriteLine(n1 - n2);
            Console.WriteLine("Matrix #1 * #2");
            Console.WriteLine(n1 * n2);
            Console.WriteLine("Matrix #1 * 2");
            Console.WriteLine(n1 * 2);
            Console.WriteLine("Matrix #1 / #2");
            Console.WriteLine(n1 / n2);
            Console.WriteLine("Matrix #1 Transpose");
            Console.WriteLine(n1.Transpose());
            Console.WriteLine("Matrix #1 Determinant");
            Console.WriteLine(n1.Determinant());
            Console.WriteLine("Matrix #1 values");
            foreach (double i in n1)
                Console.WriteLine(i);

            Console.WriteLine();
            Dictionary<int, int> coef1 = new Dictionary<int, int> { { 1, 2 }, { 2, 4 }, { 5, 6 } };
            Dictionary<int, int> coef2 = new Dictionary<int, int> { { 4, 3 }, { 3, 2 }, { 0, 1 } };
            Polynom p1 = new Polynom(coef1);
            Polynom p2 = new Polynom(coef2);

            Console.WriteLine("Polynom #1");
            Console.WriteLine(p1);
            Console.WriteLine("Polynom #2");
            Console.WriteLine(p2);
            Console.WriteLine("Polynom #1 + #2");
            Console.WriteLine(p1 + p2);
            Console.WriteLine("Polynom #1 - #2");
            Console.WriteLine(p1 - p2);
            Console.WriteLine("Polynom #1 * #2");
            Console.WriteLine(p1 * p2);
            Console.WriteLine("Polynom #1 * 2");
            Console.WriteLine(p1 * 2);
            Console.WriteLine("Polynom #1 pow 2");
            Console.WriteLine(p1.pow(2));
            Console.WriteLine("Polynom #1 list");
            foreach (KeyValuePair<int, int> el in p1)
                Console.WriteLine("{0}x^{1}", el.Value, el.Key);
            Console.WriteLine("Polynom #1 Compare to Polynom #2");
            Console.WriteLine(p1.CompareTo(p2));
            Console.WriteLine("Polynom #1 value in point 2");
            Console.WriteLine(p1.inPoint(2));
            Console.WriteLine("Polynom #1 Composition with Polynom #2");
            Console.WriteLine(p1.Сomposition(p2));


            Console.ReadKey();
        }
    }
}
