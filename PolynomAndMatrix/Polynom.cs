﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolynomAndMatrix
{
    /// <summary>
    /// Polynom class
    /// </summary>
    /// <seealso cref="System.ICloneable" />
    /// <seealso cref="System.Collections.IEnumerable" />
    /// <seealso cref="System.IComparable{PolynomAndMatrix.Polynom}" />
    class Polynom : ICloneable, IEnumerable, IComparable<Polynom>
    {
        Dictionary<int, int> data;
        int size;

        /// <summary>
        /// Initializes a new instance of the <see cref="Polynom"/> class.
        /// </summary>
        public Polynom()
        {
            Random rand = new Random();
            size = rand.Next(1, 5);
            data = new Dictionary<int, int>();
            for (int i = 0; i < size; i++)
                data.Add(rand.Next(1, 30), rand.Next(1, 30));
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Polynom"/> class.
        /// Coefficients - randomized.
        /// </summary>
        /// <param name="sz">The size (max degree).</param>
        /// <exception cref="PolynomAndMatrix.MyException">Size must be greater than 0!</exception>
        public Polynom(int sz = 5)
        {
            if (sz <= 0) throw new MyException("Size must be greater than 0!");
            Random rand = new Random();
            size = sz;
            data = new Dictionary<int, int>();
            for (int i = 0; i < size; i++)
                data.Add(rand.Next(1, 30), rand.Next(1, 30));
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Polynom"/> class.
        /// </summary>
        /// <param name="dt">The coefficients.</param>
        public Polynom(int[] dt)
        {
            size = dt.Length;
            data = new Dictionary<int, int>();
            for (int i = 0; i < size; i++)
                data.Add(i, dt[i]);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Polynom"/> class.
        /// </summary>
        /// <param name="list">The list with KeyValuePair (Key - degree; Value - coefficient).</param>
        public Polynom(Dictionary<int, int> list)
        {
            if (list.Count != 0)
                size = list.Max(x => x.Key);
            else
                throw new MyException("Size must be greater than 0!");
            data = new Dictionary<int, int>();
            for (int i = 0; i <= size; i++)
            {
                int value = list.ContainsKey(i) ? list[i] : 0;
                data.Add(i, value);
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            var ordered = data.OrderByDescending(x => x.Key);
            foreach (KeyValuePair<int, int> el in ordered)
            {
                if (el.Value == 0)
                    continue;
                if (el.Value > 0) result.Append('+');
                result.Append(el.Value);
                if (el.Key > 1) {
                    result.Append("x^");
                    result.Append(el.Key);
                }
                else if (el.Key == 1)
                    result.Append("x");
            }
            return result.ToString();
        }

        /// <summary>
        /// Implements the operator +.
        /// </summary>
        /// <param name="c1">The first Polynom.</param>
        /// <param name="c2">The second Polynom.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Polynom operator +(Polynom c1, Polynom c2)
        {
            Dictionary<int, int> list = new Dictionary<int, int>();
            foreach (KeyValuePair<int, int> el in c1)
            {
                if (list.ContainsKey(el.Key)) list[el.Key] += el.Value;
                else list.Add(el.Key, el.Value);
            }
            foreach (KeyValuePair<int, int> el in c2)
            {
                if (list.ContainsKey(el.Key)) list[el.Key] += el.Value;
                else list.Add(el.Key, el.Value);
            }
            return new Polynom(list);
        }
        /// <summary>
        /// Implements the operator -.
        /// </summary>
        /// <param name="c1">The first Polynom.</param>
        /// <param name="c2">The second Polynom.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Polynom operator -(Polynom c1, Polynom c2)
        {
            Dictionary<int, int> list = new Dictionary<int, int>();
            foreach (KeyValuePair<int, int> el in c1)
            {
                if (list.ContainsKey(el.Key)) list[el.Key] -= el.Value;
                else list.Add(el.Key, el.Value);
            }
            foreach (KeyValuePair<int, int> el in c2)
            {
                if (list.ContainsKey(el.Key)) list[el.Key] -= el.Value;
                else list.Add(el.Key, el.Value);
            }
            return new Polynom(list);
        }
        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="c1">The first Polynom.</param>
        /// <param name="c2">The second Polynom.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Polynom operator *(Polynom c1, Polynom c2)
        {
            Dictionary<int, int> list = new Dictionary<int, int>();
            foreach (KeyValuePair<int, int> el1 in c1)
            {
                foreach (KeyValuePair<int, int> el2 in c2)
                {
                    int newdegree = el1.Key + el2.Key;
                    int newvalue = el1.Value * el2.Value;
                    if (list.ContainsKey(newdegree)) list[newdegree] += newvalue;
                    else list.Add(newdegree, newvalue);
                }
            }
            return new Polynom(list);
        }
        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="c1">The Polynom.</param>
        /// <param name="c2">The number.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Polynom operator *(Polynom c1, int c2)
        {
            Dictionary<int, int> list = new Dictionary<int, int>(c1.data);
            for (int i = 0, len = list.Count; i < len; i++)
                list[i] *= c2;
            return new Polynom(list);
        }

        /// <summary>
        /// Calculates polynom value in point
        /// </summary>
        /// <param name="num">The number (value).</param>
        /// <returns>
        /// Polynom value in point.
        /// </returns>
        public int inPoint(int num)
        {
            int result = 0;
            foreach (KeyValuePair<int, int> el in data)
            {
                result += el.Value * (int)Math.Pow(num, el.Key);
            }
            return result;
        }

        /// <summary>
        /// Power up the Polynom!
        /// </summary>
        /// <param name="num">The number (value) of degree.</param>
        /// <returns>
        /// New, power upped polynom
        /// </returns>
        public Polynom pow(int num)
        {
            Dictionary<int, int> list = new Dictionary<int, int>(data);
            Polynom result = new Polynom(list);
            for (int i = 0; i < num - 1; i++)
                result *= this;
            return result;
        }

        /// <summary>
        /// Composition of polynoms
        /// </summary>
        /// <param name="num">The number (value) of degree.</param>
        /// <returns>
        /// New, power upped polynom
        /// </returns>
        public Polynom Сomposition(Polynom second)
        {
            Dictionary<int, int> list = new Dictionary<int, int>();
            for (int i = 0, len = data.Count; i < len; i++)
                list[i] = 0;
            Polynom result = new Polynom(list);
            foreach (KeyValuePair<int, int> el in data)
            {
                result += second.pow(el.Key) * el.Value;
            }
            return result;
        }

        /// <summary>
        /// Returns IEnumerator
        /// </summary>
        /// <returns>
        /// Object <see cref="T:System.Collections.IEnumerator" />
        /// </returns>
        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)data).GetEnumerator();
        }


        /// <summary>
        /// Creates copy of our <see cref="Matrix"/>.
        /// </summary>
        /// <returns>
        /// New <see cref="Matrix"/>, the copy of our <see cref="Matrix"/>.
        /// </returns>
        public object Clone()
        {
            return new Polynom(data);
        }

        /// <summary>
        /// Compares our Polynom to other Polynom.
        /// </summary>
        /// <param name="obj">The second Polynom.</param>
        /// <returns>
        /// 1, if our more than second
        /// 0, if our equals second
        /// -1, if our less than second  
        /// </returns>
        public int CompareTo(Polynom obj)
        {
            if (data.Count > obj.data.Count)
                return -1;
            else if (data.Count < obj.data.Count)
                return 1;
            else
            {
                for (int i = data.Count - 1; i != 0; i--)
                {
                    if (data[i].CompareTo(obj.data[i]) != 0)
                        return data[i].CompareTo(obj.data[i]);
                    else continue;
                }
                return 0;
            }
        }
    }
}
